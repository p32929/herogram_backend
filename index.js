const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const fs = require('fs');
const path = require('path');
const jwt = require('jsonwebtoken');
const cors = require('cors');
const mongoose = require('mongoose');

const app = express();
const PORT = process.env.PORT || 3000;

// MongoDB setup
mongoose.connect('mongodb+srv://p32929:geolHtQN3tyXEf88@cluster0.pxjppdx.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0', { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
const { Schema } = mongoose;

// Define MongoDB schemas
const userSchema = new Schema({
    username: String,
    password: String // Storing password as plain text
});
const User = mongoose.model('User', userSchema);

const fileSchema = new Schema({
    userId: Schema.Types.ObjectId,
    filename: String,
    mimetype: String,
    size: Number,
    tags: [String],
    views: { type: Number, default: 0 }
});
const File = mongoose.model('File', fileSchema);

// Middleware
app.use(bodyParser.json());
app.use(cors());
app.use(express.static('uploads')); // Folder to store uploaded files

// Authentication Middleware
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if (token == null) return res.sendStatus(401);

    jwt.verify(token, 'ACCESS_TOKEN_SECRET', (err, user) => {
        if (err) return res.sendStatus(403);
        req.user = user;
        next();
    });
}

// File Upload Middleware
const upload = multer({
    dest: 'uploads/',
    fileFilter: (req, file, cb) => {
        if (file.mimetype.startsWith('image/') || file.mimetype.startsWith('video/')) {
            cb(null, true);
        } else {
            cb(new Error('Only image or video files are allowed!'));
        }
    }
});

// Authentication Routes
app.post('/login', async (req, res) => {
    // Authenticate user
    const { username, password } = req.body;
    const user = await User.findOne({ username });
    if (!user) return res.status(400).send('User not found');

    if (password !== user.password) return res.status(403).send('Incorrect password');

    const accessToken = generateAccessToken({ username: user.username });
    res.json({ accessToken });
});

// File Management Routes
app.get('/files', authenticateToken, async (req, res) => {
    // Fetch and return list of files for the authenticated user
    const files = await File.find({ userId: req.user._id });
    res.json(files);
});

// File Management Routes
app.post('/upload', authenticateToken, upload.single('file'), async (req, res) => {
    // Process uploaded file and save metadata in the database
    const { filename, mimetype, size, originalname } = req.file;
    const tags = req.body.tags.split(',');

    // Extracting file extension
    const fileExtension = path.extname(originalname);

    // Constructing the final filename with extension
    const finalFileName = `${filename}${fileExtension}`;

    const file = new File({
        userId: req.user._id,
        filename: finalFileName,
        mimetype,
        size,
        tags
    });
    await file.save();

    res.sendStatus(200);
});


// File Sharing Routes
app.get('/view/:fileId', async (req, res) => {
    try {
        // Generate and return shareable link for the specified file
        const fileId = req.params.fileId;
        const file = await File.findById(fileId);

        // If the file is not found, return a 404 Not Found response
        if (!file) {
            return res.status(404).send("File not found");
        }

        // Increment the views count
        ++file.views;
        await file.save();

        // Get the path to the file
        const filePath = `./uploads/${file.filename.split(".")[0]}`;

        // Check if the file exists
        if (!fs.existsSync(filePath)) {
            return res.status(404).send("File not found");
        }

        // Read the file and send it as a response
        fs.readFile(filePath, (err, data) => {
            if (err) {
                return res.status(500).send("Error reading file");
            }
            // Determine the content type based on the file extension
            var contentType = ""
            if (file.filename.endsWith("png")) {
                contentType = `image/png`
            }
            else if (file.filename.endsWith("jpg")) {
                contentType = `image/jpeg`
            }
            else if (file.filename.endsWith("mp4")) {
                contentType = `video/mp4`
            }

            res.setHeader('Content-Type', contentType);
            // res.send("Image/video view count increased ");
            res.send(data);
        });
    } catch (err) {
        console.error(err);
        res.status(500).send("Internal Server Error");
    }
});

// User Creation Route
app.post('/signup', async (req, res) => {
    const { username, password } = req.body;

    // Check if username already exists
    const existingUser = await User.findOne({ username });
    if (existingUser) {
        return res.status(400).json({ error: 'Username already exists' });
    }

    // Create new user with plain text password
    const newUser = new User({
        username,
        password // Storing plain text password
    });

    try {
        await newUser.save();
        res.status(201).json({ message: 'User created successfully' });
    } catch (err) {
        console.error('Error creating user:', err);
        res.status(500).json({ error: 'Internal server error' });
    }
});

app.get('/', async (req, res) => {
    res.status(201).json({ message: 'Hello herogram' });
});

// Helper function to generate access tokens
function generateAccessToken(user) {
    return jwt.sign(user, 'ACCESS_TOKEN_SECRET', { expiresIn: '60m' });
}

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
